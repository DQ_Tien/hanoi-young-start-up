<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_2');

/** MySQL database username */
define('DB_USER', 'wordpress_2');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$RHx0ncxZ&1@%ctVb79a4G$J5c9Cu-2;CRA9v:hUh{s*LBXgEk(w_:P/=bS5TtvR');
define('SECURE_AUTH_KEY',  '@HQ><Yx1vciaM>jP5+Nk=M02E<&2mNdMna,hX,7%2~GN^dzS+qBF1XKO1%g[B7)<');
define('LOGGED_IN_KEY',    'us(7wAS$i]%DIe96b)*EIXUWEE2<Vq<%GecxiIRn~x{9P|M`Eu^6R)Lwn%C~?JpR');
define('NONCE_KEY',        'T;x%v]RL]%uN~9J&k{evKFEmE+7-_z8iWOMlCHEK6uu@W|R&He;i/f~DA&9`[y!R');
define('AUTH_SALT',        '$!}i&V#b.k>Ot(yLAuagMAz5X0qe@+rlEZhU9!BSO74KIn;xta.h($OLwXBvT:Co');
define('SECURE_AUTH_SALT', 'B%~Z4 iOH3ahvhB=,I.{b:V :+)q&tauD}Qf#PFt8pIMSsN?|S/JhVgFPS5~{l-7');
define('LOGGED_IN_SALT',   'm.-DC.p80l}(c,VI%M>Ir|QA]hJ0V2Kp*P(?BoEi4N7T`^&-Pfy|BC%p_9^Zb?W8');
define('NONCE_SALT',       ' {%)I9PwMJ^/Jd`),JSNw5Q0)<$T2gpc>*E$4%(nq&gR7L9XswYnqZFk;mV,+Lka');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
